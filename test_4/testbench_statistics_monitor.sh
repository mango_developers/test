#!/bin/bash

#######################################################################################################################
## (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
## Department of Computing Engineering (DISCA)
## Universitat Politecnica de Valencia (UPV)
## Valencia, Spain
## All rights reserved.
##                
## All code contained herein is, and remains the property of
## Parallel Architectures Group. The intellectual and technical concepts
## contained herein are proprietary to Parallel Architectures Group and
## are protected by trade secret or copyright law.
## Dissemination of this code or reproduction of this material is
## strictly forbidden unless prior written permission is obtained
## from Parallel Architectures Group.
##
## THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
## IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
## OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
##
## contact: jflich@disca.upv.es
##---------------------------------------------------------------------------------------------------------------------
##
## Company:   GAP (UPV)
## Engineer:  J. Martinez (jomarm10@gap.upv.es)
##
## Create Date: July 03, 2018
## Design Name:
## Module Name: Heterogeneous Note Testbench
## Project Name:
## Target Devices: all
## Tool Versions:
## Description:
##
##    Perform testbench on HN_system for temperature monitorization
##
##    Start hn_daemon
##    Run test for specified time in seconds
##        stop test application
##    Stop hn_daemon
##    Check test results
##
##    The application test does not communicate with bbque,
##
##    This script can optionally power-up and power-down the FPGA platfor if the user passes the parameter to the script
##
## Dependencies: NONE
##
## Revision:
##   Revision 0.01 - File Created
##
## Additional Comments: NONE
##
#######################################################################################################################

DEFAULT_PATH_PRODTOOLS=/opt/prodesign/profpga/proFPGA-2018B
DEFAULT_PATH_MANGO=/opt/mango

##---------------------------------------------------------------------------------------------------
function error() {
  printf '\E[31m'; echo "$@"; printf '\E[0m'
}

##---------------------------------------------------------------------------------------------------
function usage() { 
  echo ""
  echo "Usage: $0 [-a <architecture to test>] [-c <cluster name>] [-i <interface>] \
[-m <mango install path>] [-p <prodesign tools install path>] [ -u] [-d] [-o] [-t <time to run in seconds>]" 1>&2;
  echo "   a architecture ID, the name of the file is automatically completed: mango-archID.cfg" 1>&2;
  echo "   c cluster to boot"
  echo "      hn0 hn1 hn2 hn3 hn4 hn5"
  echo "   i physical interface between the HOST PC and the HN system" 1>&2;
  echo "       mmi64" 1>&2;
  echo "       mmi64fmpcie" 1>&2;
  echo "   m mango tools install dir"
  echo "     optional param, default path is: " $DEFAULT_PATH_MANGO
  echo "   p prodesing tools intall dir"
  echo "     optional param, default paht is: " $DEFAULT_PATH_PRODTOOLS
  echo "   u Power-Up   FPGA system. Optional, the system may be already running" 1>&2;
  echo "   d Power-Down FPGA system. Optional, the system will remain programmed" 1>&2;
  echo "   o overwrite previous testbench results folder" 1>&2;
  echo "   t time the test will run for, in seconds" 1>&2;
  echo " "
  exit 1;
}
##---------------------------------------------------------------------------------------------------
function validate_testbench() {
    RESULT=$(grep 'TEST OK' $OUTFILE_TEST)
    if [ "${RESULT:-null}" = null ]; then
        ## text string that determines that the test was ok was not found in file
        #echo "error text string ok NOT found"
        return_status=0
    else
        ## text string found, so test was succesfully run 
        #echo "fine, text string ok found"
        return_status=1
    fi
  echo $return_status
}
##---------------------------------------------------------------------------------------------------

echo ""
echo ""
echo "   TESTBENCH FOR STATISTICS MONITORIZATION"
echo "----------------------------------------------"
echo ""

OVERWRITE_ENABLE=0

while getopts "a:c:dhi:m:op:t:u" o; do
    case "${o}" in
        a)
            ARCH="${OPTARG}"
            ;;
        c)
#            CLUS_ID="${OPTARG}"
#            if [ "$CLUS_ID" \< 0 ] || [ "$CLUS_ID" \> 5 ] ; then
#              error "Cluster id #"$CLUS_ID" out of range [0,5]"
#              usage
#            fi;
#            CLUSTER="hn"$CLUS_ID
            CLUSTER="${OPTARG}"
            ;;
        d)
            POWERDOWN_DEVICE="yes"
            # option is set to "d", to keep same letter as profpga_run command option
            ;;
        h)
            usage
            ;;
        i)
            IFACE="${OPTARG}"
            if [ ${IFACE} != 'mmi64' ]; then 
              if [ ${IFACE} != 'mmi64fmpcie' ]; then 
                error "unknown interface ${IFACE}"
                usage
              fi;
            fi;
            ;;
        m)
           PATH_MANGO="${OPTARG}"
          ;;
        o)
            OVERWRITE_ENABLE=1
            ;;
        p)
            PATH_PRODTOOLS="${OPTARG}"
            ;;
        t)
            TIME_TO_RUN="${OPTARG}"
            if [[ "${TIME_TO_RUN}" < 1 ]]; then 
              error "failed time_to_run    value read is "${TIME_TO_RUN}""              
              usage
            fi;
            ;;
        u)
            BOOT_DEVICE="yes"
            # option is set to "u", to keep same letter as profpga_run command option
            ;;
        *)
            error "Unexpected parameter"
            usage
            ;;
    esac
done

shift $((OPTIND-1))

# PATHS definitions ---------------------------------------------------------------------------------
# We first check whether MANGO and PRODESIGN paths are set in the env, otherwise use default values

if [ -z "${PATH_MANGO}" ]; then
  PATH_MANGO=$DEFAULT_PATH_MANGO
  echo "setting default installation path for mango"
fi

if [ -z "${PATH_PRODTOOLS}" ]; then
  PATH_PRODTOOLS=$DEFAULT_PATH_PRODTOOLS
  echo "setting default install path for prodesign tools"
fi

PATH_ARCHS=$PATH_MANGO/usr/share/arch
PATH_ARCH=$PATH_ARCHS/"mango-arch"

PATH_BBQUE=$PATH_MANGO/bosp/var
LOGFILE_BBQUE=$PATH_BBQE/bbque.log
ARCH_CONFIG_FILE="/tmp/no_file_loaded_for_config"

LOGFILE_DAEMON="/tmp/hn_daemon.log"

PATH_TEST=`pwd`
TEST_FILE="test_stats_monitor"


# Verifications before running the testbench: options and folders
if [ -z "${IFACE}" ] || [ -z "${TIME_TO_RUN}" ] || [ -z "${ARCH}" ] || [ -z "${CLUSTER}" ]; then
    echo ""
    error "not all mandatory parameters have been initialized"
    echo "  User has to set at least"
    echo "  -a architecture ID"
    echo "  -c cluster name"
    echo "  -i interface"
    echo "  -t test execution time"
    echo ""
    usage
else
    ARCH_CONFIG_FILE=$PATH_ARCH$ARCH"/mango-arch"$ARCH"-"$CLUSTER".cfg"
    ARCH_NAME=mango-arch"$ARCH"
    #check whether file exists
    #echo "failed architecture"
    if [ ! -e "$ARCH_CONFIG_FILE" ]
    then
      ## file does not exists
      error "unknown architecture or file not found on default architectures folder"
      echo "  default path for architectures: " $PATH_ARCHS
      echo "  file not found : " $ARCH_CONFIG_FILE
      usage
    fi
fi

if [ ! -d "$PATH_MANGO" ]; then
  error "MANGO tools not found at: " $PATH_MANGO ", please check mango intallation directory"
  echo "test canceled"
  echo ""
  exit 1;
fi

if [ ! -d "$PATH_PRODTOOLS" ]; then
  error "PRODESIGN tools not found at: " $PATH_PRODTOOLS ", please check prodesign intallation directory"
  echo "test canceled"
  echo ""
  exit 1;
fi

PATH_ARCH=$PATH_ARCH"$ARCH/"
if [ ! -d "$PATH_ARCH" ]; then
  error "folder: " $PATH_ARCH "not found, check architecture exists and is placed in expected folder"
  echo "test canceled"
  echo ""
  exit 1;
fi

# check if executable test has been generated and is in place
if [ ! -e "$PATH_TEST/$TEST_FILE" ]; then
  error "Missing executable test file"
  echo "$PATH_TEST/$TEST_FILE"
  echo "test canceled"
  echo ""
  exit 1;
fi

## we currently set the outputs path to the architecture under test folder
CURR_PATH=`pwd`
PATH_OUTPUT_RELATIVE="out"
PATH_OUTPUT="$CURR_PATH/$PATH_OUTPUT_RELATIVE/$ARCH_NAME"
OUTFILE_TEST="$PATH_OUTPUT/test.out"
OUTFILE_DAEMON="$PATH_OUTPUT/daemon.out"
OUTFILE_PROD="$PATH_OUTPUT/prod.out"
OUTFILE_BBQUE="$PATH_OUTPUT/bbqe_env.out"


if [ ! -d "out" ]; then
  mkdir out
fi

## check whether testbench output folder already exists
if [ -d "$PATH_OUTPUT" ]; then
    echo ""
    echo "Destination folder for test output $PATH_OUTPUT already exists"
    if [ "$OVERWRITE_ENABLE" == 1 ]; then 
        echo "  Overwrite output folder option was selected, removing previous folder"
        rm -rf "$PATH_OUTPUT"
        if [ ! -r $PATH_OUTPUT_RELATIVE ] || [ ! -w $PATH_OUTPUT_RELATIVE ] || [ ! -x $PATH_OUTPUT_RELATIVE ]; then
            error "user has no permissions to delete previous folder $PATH_OUTPUT_RELATIVE"
            echo "test canceled"
            exit 1;
        else
            echo "    ...done"
            echo ""
        fi
    else
        KEEP_IN_LOOP=1
        while [ "$KEEP_IN_LOOP" == 1 ]
        do
            # read -p "overwrite and continue test (y) or abort test(n)? (y/N): " -n1 ans
            echo "overwrite and continue test (y) or abort test(n)? (y/N): "
            read ans
            echo ""
            if [ "$ans" == "y" ] || [ "$ans" == "Y" ]; then
                echo "  Delete previous content and continue"
                rm -rf "$PATH_OUTPUT"
                KEEP_IN_LOOP=0
                if [ ! -r $PATH_OUTPUT_RELATIVE ] || [ ! -w $PATH_OUTPUT_RELATIVE ] || [ ! -x $PATH_OUTPUT_RELATIVE ]; then
                    error "user has no permissions to delete previous folder $PATH_OUTPUT_RELATIVE"
                    echo "test canceled"
                    exit 1;
                else
                    echo "    ...done"
                    echo ""
                fi
            else    
                 if [ "$ans" == "N" ] || [ "$ans" == "n" ] || [ "$ans" == "" ]; then
                     KEEP_IN_LOOP=0
                     echo "please, move previous output folder and run test again"
                     error "test canceled"
                     echo ""
                     exit 1;
                 fi
            fi
        done
    fi
fi

mkdir -p "$PATH_OUTPUT"

if [ ! -d "$PATH_OUTPUT" ]; then
    error "output folder not created"
    echo "please check rw permissions for user in target folder"
    echo "    $CURR_PATH"
    echo "unable to write outputs"
    echo "test canceled"
    echo ""
    exit 1;
fi

if [ ! -r $PATH_OUTPUT_RELATIVE ] || [ ! -w $PATH_OUTPUT_RELATIVE ] || [ ! -x $PATH_OUTPUT_RELATIVE ]; then
  error "user has not full access permissions to output folder $PATH_OUTPUT_RELATIVE"
  echo "test canceled"
  exit 1;
fi


##---------------------------------------------------------------------------------------------------------------------
##---------------------------------------------------------------------------------------------------------------------
echo "test output folder: " $PATH_OUTPUT
echo

##before do anything, check whether the daemon is running...
RESULT=`pidof hn_daemon`
if [ "${RESULT:-null}" != null ]; then
    error "ERROR hn_daemon is already running"
    echo "  abort testbench to prevent interact with other applications"
    echo "testbench will exit now"
    echo
    exit 1;
fi
## check whether test application link exists 
if [ ! -e $TEST_FILE ]; then
    error "ERROR $TEST_FILE application not found in current folder"
    echo "   test canceled"
    echo "" 
fi


## We remove the log file for BBQUE
#echo 
#echo "removing previous bbque log"
#rm -f $PATH_BBQUE/bbque.log
#echo "  ...done"

##
## the daemon is not running, and the test application existes, proceed with the test

# We set the PROD TOOLS environment
echo "setting proDesign environment variables"
. $PATH_PRODTOOLS/bin/settings64.sh
echo "  ...done"

# We program the FPGAs, only if option was selected
if [ ! -z "$BOOT_DEVICE" ]; then
    echo
    echo "Programming HN_system with file at " "$ARCH_CONFIG_FILE"
    echo "   this step may take a while, so please be patient...."
    cd $PATH_ARCH
    profpga_run "$ARCH_CONFIG_FILE" -u >> $OUTFILE_PROD

    # wait until fpga has been programmed
    wait
    echo "  ...done"
else
    echo "skipping step: Will NOT program the FPGA, it is expected to be running"
    echo ""
fi

# We launch the HN daemon
echo "launching HN daemon"
cd $PATH_MANGO/bin
./hn_daemon -c "$CLUSTER" -i "$IFACE" >> $OUTFILE_DAEMON &
sleep 15
echo "  ...done"
echo ""

## daemon is expected to be running, we check it
RESULT=`pidof hn_daemon`
if [ "${RESULT:-null}" = null ]; then
    error "ERROR hn_daemon is not running"
    echo "test canceled"
    echo ""
    exit 1;
else
  echo "hn_daemon running with PID $RESULT"
  echo ""
fi

# We do NOT launch the BBQUE environment
## We launch the BBQUE
#echo 
#echo "Launching BBQUE"
#cd $PATH_MANGO/bosp
#source etc/bbque/bosp_init.env >> $OUTFILE_BBQUE
#bbque-startd --nocg >> $OUTFILE_BBQUE
##sleep 15
#wait
#echo "  ...done"
## bbque is expected to be running

#
## We are ready, let's proceed with the test
cd $PATH_TEST
echo "Starting test"
echo "calling statistics access and monitorization test, running for $TIME_TO_RUN seconds"
echo ""

echo "Starting test" >> $OUTFILE_TEST
echo "calling statistics access and monitorization test, running for $TIME_TO_RUN seconds" >> $OUTFILE_TEST
echo "" >> $OUTFILE_TEST

#### WARINING WE SHOULD CHECK HOW MANY TILES THE SYSTEM HAS, AND THEN SET THE POLLING INTERVAL ACCORDINGLY
./$TEST_FILE -e -p 300 -d 2 -s >> $OUTFILE_TEST &

sleep 1
RESULT=`pidof $TEST_FILE`
if [ "${RESULT:-null}" = null ]; then
    error "ERROR $TEST_FILE is not running"
    echo "ERROR $TEST_FILE is not running" >> $OUTFILE_TEST
    # We stop the HN daemon
    echo 
    echo "    stopping HN daemon" >> $OUTFILE_TEST
    cd $PATH_MANGO/bin
    #cd /home/jomarm10/repos/hn-lib/hn_daemon
    ./hn_daemon_stop.sh >> $OUTFILE_DAEMON
    wait
    echo "      ...done" >> $OUTFILE_TEST
    echo "test canceled" >> $OUTFILE_TEST
    echo "test canceled"
    echo
    exit 1;
else
    echo "Test is running with PID $RESULT"  >> $OUTFILE_TEST
    echo "Test is running with PID $RESULT"
    TSTART=1
    TEND=$TIME_TO_RUN
    vi=$TSTART
#    sleep $TIME_TO_RUN
    echo "  remaining $TEND seconds                                      "
    sleep 1
    while [[ $vi -le $TEND ]]; do
      vpr=`expr "$TEND" - "$vi"`
      printf '\033[A\33[2K\r'; echo "  remaining $vpr seconds                                      "
      vi=`expr "$vi" + 1`
      sleep 1
    done
    printf '\033[A\33[2K\r'; echo "  remaining 0 seconds                                      "
    echo
    echo "stopping application" >> $OUTFILE_TEST
    echo "stopping application"
    kill -2 $RESULT
fi
sleep 2


# Test finish, we start the disconnection process
## We stop BBQUE
#echo
#echo "Stopping BBQUE"
#bbque-stopd >> $OUTFILE_BBQUE
#echo "  ...done"

#we append the daemon log file to the daemon output file
cat $LOGFILE_DAEMON >> $OUTFILE_DAEMON

# We stop the HN daemon
echo ""
echo "start finizalitation tasks" >> $OUTFILE_TEST
echo "start finizalitation tasks"
echo ""  >> $OUTFILE_TEST
echo "Stopping HN daemon" >> $OUTFILE_TEST
echo "Stopping HN daemon"
cd $PATH_MANGO/bin
#cd /home/jomarm10/repos/hn-lib/hn_daemon
./hn_daemon_stop.sh >> $OUTFILE_DAEMON
echo "  ...done" >> $OUTFILE_TEST
echo "  ...done"
# wait a few seconds for the daemon to release the mmi64 interface
sleep 5

# We power off FPGAs, only if option was selected
if [ ! -z "$POWERDOWN_DEVICE" ]; then
    echo "" >> $OUTFILE_TEST
    echo ""
    echo "Powering down FPGAs" >> $OUTFILE_TEST
    echo "Powering down FPGAs"
    cd $PATH_ARCH
    profpga_run "$ARCH_CONFIG_FILE" -d >> $OUTFILE_PROD
    wait
    echo "  ...done"
    echo "  ...done" >> $OUTFILE_TEST
else
    echo "Skipping step: Will NOT power down the FPGA"
    echo "Skipping step: Will NOT power down the FPGA" >> $OUTFILE_TEST
    echo ""
    echo "" >> $OUTFILE_TEST
fi

#cp $PATH_BBQUE/bbque.log $PATH_OUTPUT/bbqe.log

echo
echo "Test outputs available in " $PATH_OUTPUT
ls -l $PATH_OUTPUT

if [ -e "$PATH_OUTPUT_TEST" ]; then
    echo 
    error "UNEXPECTED ERROR"
    echo "test output file not found at output folder"
    echo ""
    exit 1;
else
    echo "Check test results"  >> $OUTFILE_TEST
    echo "Check test results"
    echo ""  >> $OUTFILE_TEST
    echo ""

    RESULT="$(validate_testbench $PATH_OUTPUT_FILE)"
    if [[ "$RESULT" != 1 ]]; then
        echo "ERROR: TEST FAILED !!"
    else
        echo "TEST SUCCESSFULLY PASSED"
    fi
fi

echo "" 
echo "" >> $OUTFILE_TEST
echo "end of testbench"
echo "end of testbench" >> $OUTFILE_TEST
echo "" >> $OUTFILE_TEST
echo ""

#######################################################################################################################
## end of file
#######################################################################################################################

