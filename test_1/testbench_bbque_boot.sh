#!/bin/bash

#######################################################################################################################
## (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
## Department of Computing Engineering (DISCA)
## Universitat Politecnica de Valencia (UPV)
## Valencia, Spain
## All rights reserved.
##                
## All code contained herein is, and remains the property of
## Parallel Architectures Group. The intellectual and technical concepts
## contained herein are proprietary to Parallel Architectures Group and
## are protected by trade secret or copyright law.
## Dissemination of this code or reproduction of this material is
## strictly forbidden unless prior written permission is obtained
## from Parallel Architectures Group.
##
## THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
## IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
## OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
##
## contact: jflich@disca.upv.es
##---------------------------------------------------------------------------------------------------------------------
##
## Company:   GAP (UPV)
## Engineer:  J. Martinez (jomarm10@gap.upv.es)
##
## Create Date: may 25, 2018
## Design Name:
## Module Name: Heterogeneous Note Testbend
## Project Name:
## Target Devices: GN0
## Tool Versions:
## Description:
##
##    Perform testbench on GN0 for bbque start-stop process
##
##    Start hn_daemon
##    Loop:
##      Start BBQUE environment
##      Stop BBQUE
##    Stop hn_daemon
##    Check test results
##
##    This script can optionally power-up and power-down the FPGA platfor if the user passes the parameter to the script
##
## Dependencies: NONE
##
## Revision:
##   Revision 0.01 - File Created
##
## Additional Comments: NONE
##
#######################################################################################################################

DEFAULT_PATH_PRODTOOLS=/opt/prodesign/profpga/proFPGA-2018B
DEFAULT_PATH_MANGO=/opt/mango

##---------------------------------------------------------------------------------------------------
function error() {
  printf '\E[31m'; echo "$@"; printf '\E[0m'
}

##---------------------------------------------------------------------------------------------------
function usage() { 
  echo ""
  echo "Usage: $0 [-a <architecture to test>] [-c <cluster name>] [-i <interface>] \
[-m <mango install path>] [-p <prodesign tools install path>] [ -u] [-d] [-n <number of runs>] [-o]" 1>&2; 
  echo "   a architecture ID, the name of the file is automatically completed: mango-archID.cfg" 1>&2;
  echo "   c cluster to boot"
  echo "      hn0 hn1 hn2 hn3 hn4 hn5"
  echo "   i physical interface between the HOST PC and the HN system" 1>&2;
  echo "       mmi64" 1>&2;
  echo "       mmi64fmpcie" 1>&2;
  echo "   m mango tools install dir"
  echo "     optional param, default path is: " $DEFAULT_PATH_MANGO
  echo "   p prodesing tools intall dir"
  echo "     optional param, default paht is: " $DEFAULT_PATH_PRODTOOLS
  echo "   u Power-Up   FPGA system. Optional, the system may be already running" 1>&2;
  echo "   d Power-Down FPGA system. Optional, the system will remain programmed" 1>&2;
  echo "   o overwrite previous testbench results folder" 1>&2;
  echo "   n number of times that bbque env will be started and stopped" 1>&2;
  echo " "
  echo "WARNNG: This testbench has to be run as sudo, this test runs applications in background that need root permissions"
  echo " "
  exit 1; 
}
##---------------------------------------------------------------------------------------------------
function validate_testbench() {
    RESULT=$(grep 'TEST OK' $OUTFILE_TEST)
    if [ "${RESULT:-null}" = null ]; then
        ## text string that determines that the test was ok was not found in file
        #echo "error text string ok NOT found"
        return_status=0
    else
        ## text string found, so test was succesfully run 
        #echo "fine, text string ok found"
        return_status=1
    fi
  echo $return_status
}
##---------------------------------------------------------------------------------------------------

echo ""
echo "------------------------------------"
echo "   TESTBENCH FOR BBQUE ENVIRONMENT   "
echo "------------------------------------"
echo ""

OVERWRITE_ENABLE=0

while getopts "a:c:dhi:m:n:op:u" o; do
    case "${o}" in
        a)
            ARCH="${OPTARG}"
            ;;
        c)
#            CLUS_ID="${OPTARG}"
#            if [ "$CLUS_ID" \< 0 ] || [ "$CLUS_ID" \> 5 ] ; then
#              error "Cluster id #"$CLUS_ID" out of range [0,5]"
#              usage
#            fi;
#            CLUSTER="hn"$CLUS_ID
             CLUSTER="${OPTARG}"
            ;;
        d)
            POWERDOWN_DEVICE="yes"
            # option is set to "d", to keep same letter as profpga_run command option
            ;;
        h)
            usage
            ;;
        i)
            IFACE="${OPTARG}"
            if [ ${IFACE} != 'mmi64' ]; then 
              if [ ${IFACE} != 'mmi64fmpcie' ]; then 
                error "unknown interface ${IFACE}"
                usage
              fi;
            fi;
            ;;
        m)
           PATH_MANGO="${OPTARG}"
          ;;
        n)
            NUMBER_OF_RUNS="${OPTARG}"
            if [[ "${NUMBER_OF_RUNS}" < 1 ]]; then 
              error "failed NUMBER of Times that BBQUE environment will be started-stopped    value read is  ${NUMBER_OF_RUNS}"
              usage
            fi;
            ;;
        o)
            OVERWRITE_ENABLE=1
            ;;
        p)
            PATH_PRODTOOLS="${OPTARG}"
            ;;
        u)
            BOOT_DEVICE="yes"
            # option is set to "u", to keep same letter as profpga_run command option
            ;;
        *)
            error "Unexpected parameter"
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if (( $EUID != 0 )); then
    error "Please run as root"
    echo ""
    exit 1;
fi

# PATHS definitions ---------------------------------------------------------------------------------
# We first check whether MANGO and PRODESIGN paths are set, otherwise use default values

if [ -z "${PATH_MANGO}" ]; then
  PATH_MANGO=$DEFAULT_PATH_MANGO
  echo "setting default installation path for mango"
fi

if [ -z "${PATH_PRODTOOLS}" ]; then
  PATH_PRODTOOLS=$DEFAULT_PATH_PRODTOOLS
  echo "setting default install path for prodesign tools"
fi

PATH_ARCHS=$PATH_MANGO/usr/share/arch
PATH_ARCH=$PATH_ARCHS/"mango-arch"

PATH_BBQUE=$PATH_MANGO/bosp/var
LOGFILE_BBQUE=$PATH_BBQUE/bbque.log
ARCH_CONFIG_FILE="/tmp/no_file_loaded_for_config"

LOGFILE_DAEMON="/tmp/hn_daemon.log"

PATH_TEST=`pwd`

# Verifications before running the testbench: options and folders
if [ -z "${IFACE}" ] || [ -z "${NUMBER_OF_RUNS}" ] || [ -z "${ARCH}" ] || [ -z "${CLUSTER}" ] ;
then
    echo ""
    error "not all mandatory parameters have been initialized"
    echo "  User has to set at least"
    echo "  -a architecture ID"
    echo "  -c cluster name"
    echo "  -i interface"
    echo "  -n Number of times that bbque-start and stop process will be triggered"
    echo ""
    usage
else
    ARCH_CONFIG_FILE=$PATH_ARCH$ARCH"/mango-arch"$ARCH"-"$CLUSTER".cfg"
    ARCH_NAME=mango-arch"$ARCH"
    #check whether file exists
    #echo "failed architecture"
    if [ ! -e "$ARCH_CONFIG_FILE" ]
    then
      ## file does not exists
      error "unknown architecture or file not found on default architectures folder"
      echo "  default path for architectures: " $PATH_ARCHS
      echo "  file not found : " $ARCH_CONFIG_FILE
      usage
    fi
fi

if [ ! -d "$PATH_MANGO" ]; then
  error "MANGO tools not found at: " $PATH_MANGO ", please check mango intallation directory"
  echo "test canceled"
  echo ""
  exit 1;
fi

if [ ! -d "$PATH_PRODTOOLS" ]; then
  error "PRODESIGN tools not found at: " $PATH_PRODTOOLS ", please check prodesign intallation directory"
  echo "test canceled"
  echo ""
  exit 1;
fi

PATH_ARCH=$PATH_ARCH"$ARCH/"
if [ ! -d "$PATH_ARCH" ]; then
  error "folder: " $PATH_ARCH "not found, check architecture exists and is placed in expected folder"
  echo "test canceled"
  echo ""
  exit 1;
fi

# check if executable test has been generated and is in place
if [ ! -e "$PATH_TEST/$TEST_FILE" ]; then
  error "Missing executable test file"
  echo "$PATH_TEST/$TEST_FILE"
  echo "test canceled"
  echo ""
  exit 1;
fi

## we currently set the outputs path to the architecture under test folder
CURR_PATH=`pwd`
PATH_OUTPUT_RELATIVE="out"
PATH_OUTPUT="$CURR_PATH/$PATH_OUTPUT_RELATIVE/$ARCH_NAME"
OUTFILE_TEST="$PATH_OUTPUT/test.out"
OUTFILE_DAEMON="$PATH_OUTPUT/daemon.out"
OUTFILE_PROD="$PATH_OUTPUT/prod.out"
OUTFILE_BBQUE="$PATH_OUTPUT/bbque_log.out"
OUTFILE_RX_TERM="$PATH_OUTPUT/rx_term.out"

RX_TERM_TMP="$PATH_OUTPUT/rx_term.tmp"
PEAK_IN_ARCH_DETECTION_FILE="$PATH_OUTPUT/peaks_in_arch.tmp"
PEAK_BOOT_DETECTION_FILE="$PATH_OUTPUT/peaks_boot.tmp"
#TimeToWaitSeconds per peak tile during boot process
TTWS_PER_PEAK_TILE=3
EXPECTED_NUMBER_OF_PEAK_TILES=0

## PEAK_SUCCESSFUL_INITIALIZATION MESSAGES
declare -a psi_array=("Welcome to PEAK architecture"
                     "Architecture ID" 
                     "Num cores"
                     "Initialising memory banks.."
                     "Initialising process list..."
                     "Initialising slaves.."
                     )

if [ ! -d "out" ]; then
  mkdir out
fi

## check whether testbench output folder already exists
if [ -d "$PATH_OUTPUT" ]; then
    echo ""
    echo "Destination folder for test output $PATH_OUTPUT already exists"
    if [ "$OVERWRITE_ENABLE" == 1 ]; then 
        echo "  Overwrite output folder option was selected, removing previous folder"
        rm -rf "$PATH_OUTPUT"
        if [ ! -r $PATH_OUTPUT_RELATIVE ] || [ ! -w $PATH_OUTPUT_RELATIVE ] || [ ! -x $PATH_OUTPUT_RELATIVE ]; then
            error "user has no permissions to delete previous folder $PATH_OUTPUT_RELATIVE"
            echo "test canceled"
            exit 1;
        else
            echo "    ...done"
            echo ""
        fi
    else
        KEEP_IN_LOOP=1
        while [ "$KEEP_IN_LOOP" == 1 ]
        do
            # read -p "overwrite and continue test (y) or abort test(n)? (y/N): " -n1 ans
            echo "overwrite and continue test (y) or abort test(n)? (y/N): "
            read ans
            echo ""
            if [ "$ans" == "y" ] || [ "$ans" == "Y" ]; then
                echo "  Delete previous content and continue"
                rm -rf "$PATH_OUTPUT"
                KEEP_IN_LOOP=0
                if [ ! -r $PATH_OUTPUT_RELATIVE ] || [ ! -w $PATH_OUTPUT_RELATIVE ] || [ ! -x $PATH_OUTPUT_RELATIVE ]; then
                    error "user has no permissions to delete previous folder $PATH_OUTPUT_RELATIVE"
                    echo "test canceled"
                    exit 1;
                else
                    echo "    ...done"
                    echo ""
                fi
            else    
                 if [ "$ans" == "N" ] || [ "$ans" == "n" ] || [ "$ans" == "" ]; then
                     KEEP_IN_LOOP=0
                     echo "please, move previous output folder and run test again"
                     error "test canceled"
                     echo ""
                     exit 1;
                 fi
            fi
        done
    fi
fi

mkdir -p "$PATH_OUTPUT"

if [ ! -d "$PATH_OUTPUT" ]; then
    error "output folder not created"
    echo "please check rw permissions for user in target folder"
    echo "    $CURR_PATH"
    echo "unable to write outputs"
    echo "test canceled"
    echo ""
    exit 1;
fi

if [ ! -r $PATH_OUTPUT_RELATIVE ] || [ ! -w $PATH_OUTPUT_RELATIVE ] || [ ! -x $PATH_OUTPUT_RELATIVE ]; then
  error "user has not full access permissions to output folder $PATH_OUTPUT_RELATIVE"
  echo "test canceled"
  exit 1;
fi


##---------------------------------------------------------------------------------------------------------------------
##---------------------------------------------------------------------------------------------------------------------
echo "test output folder: " $PATH_OUTPUT
echo

##before do anything, check whether the daemon is running...
RESULT=`pidof hn_daemon`
if [ "${RESULT:-null}" != null ]; then
    error "ERROR hn_daemon is already running"
    echo "  abort testbench to prevent interact with other applications"
    echo "testbench will exit now"
    echo
    exit 1;
fi
## check whether bbque is running...
RESULT=`pidof barbeque`
if [ "${RESULT:-null}" != null ]; then
    error "ERROR bbque is already running"
    echo "  abort testbench to prevent interact with other applications"
    echo "testbench will exit now"
    echo ""
    exit 1;
fi
## check whether rx_term application link exists 
if [ ! -e rx_term ]; then
    error "ERROR rx application not found in current folder"
    echo "  abort testbench"
    echo "testbench will exit now"
    echo "" 
fi

##
## Nor the daemon nor bbque are running, proceed with the test

# We set the PROD TOOLS environment
echo "setting proDesign environment variables"
. $PATH_PRODTOOLS/bin/settings64.sh
echo "  ...done"
echo ""

# We program the FPGAs, ONLY if this option was selected
echo "Programming HN_system with file at " "$ARCH_CONFIG_FILE"
if [ ! -z "$BOOT_DEVICE" ]; then
    echo "   this step may take a while, so please be patient...."
    echo ""
    cd $PATH_ARCH
    profpga_run "$ARCH_CONFIG_FILE" -u >> $OUTFILE_PROD

    # wait until fpga has been programmed
    wait
    echo "  ...done"
    echo ""
else
    echo "   skipping step: Will NOT program the FPGA, it is expected to be running"
    echo ""
fi

# We launch the HN daemon
echo "launching HN daemon"
cd $PATH_MANGO/bin
./hn_daemon -c "$CLUSTER" -i "$IFACE" >> $OUTFILE_DAEMON &
sleep 15
echo "  ...done"
echo ""

## daemon is expected to be running, we check it
RESULT=`pidof hn_daemon`
if [ "${RESULT:-null}" = null ]; then
    error "ERROR hn_daemon is not running"
    echo "test canceled"
    echo ""
    exit 1;
else
  echo "hn_daemon running with PID $RESULT"
  echo ""
fi

## We are ready, we can now proceed with the test
#
NUMBER_OF_ERROR_RUNS=0

# Let's prepare the environment to run bbque
cd $PATH_MANGO/bosp
source etc/bbque/bosp_init.env >> $OUTFILE_BBQUE

echo "Starting test"
echo "booting & stopping bbque $NUMBER_OF_RUNS times"
echo ""

echo "Starting test" >> $OUTFILE_TEST
echo "booting & stopping bbque $NUMBER_OF_RUNS times" >> $OUTFILE_TEST
echo "" >> $OUTFILE_TEST

for (( it=1; it<=$NUMBER_OF_RUNS; it++ )); do
    # We remove the log file for BBQUE at the begining of the loop, we will attach it to the bbque output file end of each iteration
    rm -f $LOGFILE_BBQUE
    
    echo "ITERATION " $it 
    echo "" >> $OUTFILE_BBQUE
    echo "" >> $OUTFILE_BBQUE
    echo "ITERATION " $it >> $OUTFILE_BBQUE
    echo "---------------------" >> $OUTFILE_BBQUE
    echo "" >> $OUTFILE_BBQUE

    # We launch rx_term to capture peak processors messages
    # no previous instance of rx_term running with hnlib will be running since the daemon was off
    ## we need stdout to be inmediately written into the files...
    echo "  launching rx_term" >> $OUTFILE_TEST
    echo "" >> $OUTFILE_TEST
    echo "" >> $OUTFILE_TEST
    echo "ITERATION " $it >> $OUTFILE_TEST
    echo "  launching rx_term" >> $OUTFILE_TEST

    cd $CURR_PATH
    ##This disables stdout buffering altogether, we redirect to rx_term_tmp overwriting possible existing file:
    stdbuf -o0 ./rx_term -m 0 -t 999 -d hnmem -hnlib > $RX_TERM_TMP &
#    ./rx_term -m 0 -t 999 -hnlib >> $RX_TERM_TMP &
    sleep 1
    echo "  ...done" >> $OUTFILE_TEST
    RESULT=`pidof rx_term`
    if [ "${RESULT:-null}" = null ]; then
        error "ERROR rx_term is not running"
        echo "ERROR rx_term is not running" >> $OUTFILE_TEST
        # We stop the HN daemon
        echo 
        echo "    stopping HN daemon" >> $OUTFILE_TEST
        cd $PATH_MANGO/bin
        ./hn_daemon_stop.sh >> $OUTFILE_DAEMON
        wait
        echo "      ...done" >> $OUTFILE_TEST
        echo "test canceled" >> $OUTFILE_TEST
        echo "test canceled"
        echo
        exit 1;
    fi


    # We launch the BBQUE
    echo "Run #" $it "/" $NUMBER_OF_RUNS >> $OUTFILE_TEST
    echo "Launching BBQUE" >> $OUTFILE_TEST
    bbque-startd --nocg >> $OUTFILE_BBQUE &
    echo "" >> $OUTFILE_TEST
    sleep 2

    ## bbque is expected to be running
    # get pid of process or call bbque-pid, will retur id or error message
    ## at this point we check that bbque is runnin
    RESULT=`pidof barbeque`
    if [ "${RESULT:-null}" = null ]; then
        error "ERROR bbque is not running"
        echo "ERROR bbque is not running" >> $OUTFILE_TEST
        echo "" >> $OUTFILE_TEST
        NUMBER_OF_ERROR_RUNS=$((NUMBER_OF_ERROR_RUNS+1))
    else 
        echo "  ...done" >> $OUTFILE_TEST
        pid_of_bbque=`pidof barbeque`
        echo "  bbque is running with PID " $RESULT >> $OUTFILE_TEST
        echo "" >> $OUTFILE_TEST
        echo "  bbque is running with PID " $RESULT
        # after loading the daemon, we opened a rx_term app, with fileter set to all tiles, and we redirected the output to a file, so we can now check for the initialization of the peak tiles
        # for this, let's read bbque-log file, scanning for peak tiles,
        echo "  waiting 10 secs for the system to be initialized (bbque resets architecture....)" >> $OUTFILE_TEST
        sleep 10
      
        peakosInside=$(cat $LOGFILE_BBQUE | grep -c "of family PEAK and model")    

        if [ "$it" != 1 ]; then
            if [ "$EXPECTED_NUMBER_OF_PEAK_TILES" != "$peakosInside" ]; then
                error "ERROR mismatch in number of PEAK tiles, previous "$EXPECTED_NUMBER_OF_PEAK_TILES " , current " $peakosInside >> $OUTFILE_TEST
                echo >> $OUTFILE_TEST
                ERROR_DETECTED_IN_PEAKOS_BOOTS=1
            fi
        fi
        EXPECTED_NUMBER_OF_PEAK_TILES=$peakosInside

        if [ "$peakosInside" != 0 ]; then
            ERROR_DETECTED_IN_PEAKOS_BOOTS=0
            echo "  Detected "$peakosInside" PEAK tiles" >> $OUTFILE_TEST
            TIME_TO_WAIT_SECS=$(( peakosInside * TTWS_PER_PEAK_TILE ))
            echo "  waiting "$TIME_TO_WAIT_SECS" secs for all tiles to be booted" >> $OUTFILE_TEST
            sleep $TIME_TO_WAIT_SECS
            # check that all peak tiles booted

            #we stop the rx_term and add the output to the rx_term out file
            echo "  stopping rx_term" >> $OUTFILE_TEST
#            echo "  stopping rx_term"
            RESULT=`pidof rx_term`
            kill -2 $RESULT
#            echo "  ...done"
            cat $RX_TERM_TMP >> $OUTFILE_RX_TERM
            echo "  ...done" >> $OUTFILE_TEST



#            echo "vaig a imprimir per pantalla el contingut del fitxer de rx_term_tmp"
#            cat $RX_TERM_TMP
#            echo ""
#            echo "final del fitxer"

            for i in "${psi_array[@]}"; do
                TXT_STR_MATCHES=`grep -w "$i" -c $RX_TERM_TMP`
                if (( "$TXT_STR_MATCHES" != "$peakosInside" )); then
                    echo "Error not all PEAK processors were succesfully booted, detected " $TXT_STR_MATCHES  " matches, expected " $peakosInside" for txt_str " $i >> $OUTFILE_TEST
                    ERROR_DETECTED_IN_PEAKOS_BOOTS=1
                fi
            done
        else
            echo "  NO PEAK tiles detected in architecure " $ARCH >> $OUTFILE_TEST
        fi

        if [ "$ERROR_DETECTED_IN_PEAKOS_BOOTS" != 0 ]; then
            ERROR_DETECTED_IN_PEAKOS_BOOTS=0
            NUMBER_OF_ERROR_RUNS=$((NUMBER_OF_ERROR_RUNS+1))
        else
            echo "PEAKOS successfully booted" >> $OUTFILE_TEST
        fi
       
        # We stop BBQUE
        echo >> $OUTFILE_TEST
        echo "Stopping BBQUE" >> $OUTFILE_TEST
        echo "Stopping BBQUE"  >> $OUTFILE_TEST
        bbque-stopd >> $OUTFILE_BBQUE &
        echo "  ...done" >> $OUTFILE_TEST
        sleep 2
    fi
#    # we wait one second and start next loop
    sleep 1

     #remove tmp file
    rm -f $RX_TERM_TMP


    # we attach the bbque log file 
    cat $LOGFILE_BBQUE >> $OUTFILE_BBQUE
    #we give time to the daemon to detect that the app has been killed
    sleep 5

    echo "" >> $OUTFILE_TEST
    echo "end of iteration" >> $OUTFILE_TEST
    echo "" >> $OUTFILE_TEST
done

# Test finish, we start the disconnection process

#we append the daemon log file to the daemon output file
cat $LOGFILE_DAEMON >> $OUTFILE_DAEMON

# We stop the HN daemon
echo ""
echo "All iterations done" >> $OUTFILE_TEST
echo "All iterations done"
echo "start finizalitation tasks" >> $OUTFILE_TEST
echo "start finizalitation tasks"
echo ""  >> $OUTFILE_TEST
echo "Stopping HN daemon" >> $OUTFILE_TEST
echo "Stopping HN daemon"
cd $PATH_MANGO/bin
#cd /home/jomarm10/repos/hn-lib/hn_daemon
./hn_daemon_stop.sh >> $OUTFILE_DAEMON
echo "  ...done" >> $OUTFILE_TEST
echo "  ...done"
# wait a few seconds for the daemon to release the mmi64 interface
sleep 5

# We power off FPGAs, only if option was selected
if [ ! -z "$POWERDOWN_DEVICE" ]; then
    echo "" >> $OUTFILE_TEST
    echo ""
    echo "Powering down FPGAs" >> $OUTFILE_TEST
    echo "Powering down FPGAs"
    cd $PATH_ARCH
    profpga_run "$ARCH_CONFIG_FILE" -d >> $OUTFILE_PROD
    wait
    echo "  ...done"
    echo "  ...done" >> $OUTFILE_TEST
else
    echo "Skipping step: Will NOT power down the FPGA"
    echo "Skipping step: Will NOT power down the FPGA" >> $OUTFILE_TEST
    echo ""
    echo "" >> $OUTFILE_TEST
fi

echo
echo "Test outputs available in " $PATH_OUTPUT
ls -l $PATH_OUTPUT

if [ -e "$PATH_OUTPUT_TEST" ]; then
    echo 
    error "UNEXPECTED ERROR"
    echo "test output file not found at output folder"
    echo ""
    exit 1;
else
    echo "Check test results"  >> $OUTFILE_TEST
    echo "Check test results"
    echo ""  >> $OUTFILE_TEST
    echo ""

    #RESULT="$(validate_testbench $PATH_OUTPUT_FILE)"
    if [[ "$NUMBER_OF_ERROR_RUNS" != 0 ]]; then
        echo "ERROR: TEST FAILED !!"
        echo "ERROR: TEST FAILED !!" >> $OUTFILE_TEST
    else
        echo "TEST SUCCESSFULLY PASSED"
        echo "TEST SUCCESSFULLY PASSED" >> $OUTFILE_TEST
    fi
fi

echo "" 
echo "" >> $OUTFILE_TEST
echo "end of testbench"
echo "end of testbench" >> $OUTFILE_TEST
echo "" >> $OUTFILE_TEST
echo ""

#######################################################################################################################
## end of file
#######################################################################################################################

