# NOTE: to run the testbenches it is recommended to compile and run the hn_lib and the hn_daemon with NO debug in order to reduce the size of the resulting test output file. 
#        Otherwise the size of the resulting output file may not fit in the HDD
# NOTE: After a fresh install, and before running the testbechens, please first run BBQUE to configure the environent, otherwise the testbenches will fail

# test 1 test bbque start, load peakos, stop bbque
# test 2 test temperature monitorization
# test 3 test external access to mango regbank (test performed over available regs from the hn_lib)
# test 4 test statistics monitorization
# test 5 test shared memory burst transfers in both directions, checks data
